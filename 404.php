<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

get_header(); ?>

<div class="content">
    <!--/ Start 404 Notice /-->
    <div class="notfound">
    	<h1>Something Broke!</h1>
        <p>Well this is embarrassing. It turns out the page or file you were looking for is missing (or never existed)!</p>
        <p>You could try hitting <a href="<?php echo home_url(); ?>">the home page</a> or see if you can find what you are looking for in the menu.</p>
    </div>
    <!--/ End 404 Notice /-->
</div>

<?php get_footer(); ?>
