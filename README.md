# Stormbox Theme
A starter Wordpress theme for Stormbox websites.

## Change Log

### 1.6.3
- Add a filter to wrap YouTube embeds in a container to allow for easy responsive styling
- Cleaned up some attributes and indentation

### 1.6.2
- Add Gulp support back with revised gulpfile.js
- Removed home page "options menu" for ACF
- Added functions to remove emoji styles and scripts

### 1.6.1
- Removed 404 graphic and styling.
- Replaced css reset with a combo of the existing reset and normalize, moved to partial.
- Added new mixins.
- Removed "h1" tag usually used for logo in favour of an "a" tag.
- Add box-sizing: border-box;.
- Remove Gulp support for now, due to compatibility issues.
- Add a css query string toggle css=no

### 1.6
- Added several conditions for administrators.
- Fixed issue of missing stormbox logo login image.
- Option to allow custom CSS in the visual editor.
- Option to enable maintenance mode.
- Option for custom maintenance mode message.
- Update icon references and image sizes
- Added gulp support
- Updated jQuery enqueue
- Added HTML5 Shiv
- Updated search form to use "get_search_form" function rather than inline markup
- Added Home Page custom "Options Menu" item when using ACF

### 1.5.9
- Added web app capable views.
- Added Stormbox settings page.
- Added Google Analytics placeholder.
- Included home page template (home.php).
- Commented out additional image sizes function.
- Removed function for displaying additional image sizes in admin when inserting photo into page/post (no longer works).

### 1.5.8
- Added function to disable automatic updates.

### 1.5.7
- Added extra args to wp_nav_menu array.

### 1.5.6
- Removed stormbox-options.php
- Removed functions (are now part of a dedicated stormbox plugin)

### 1.5.5
- Removed mq.css
- Enqueued scripts and styles through 'functions.php'
- Added cachebuster to scripts and styles based on file modified date
- Added mixins for media query breakpoints
- Updated menu support to allow for Primary, Sidebar and Footer Navigation + updated the code used to activate and include them
- Replaced div id's with classes
- Removed mobile menu helper
- Added latest version of modernizr - 2.7.1
- jQuery updated to version - 1.10.2
- Added compass support
- Fixed favicon path

### 1.5.4
- Added support for new Stormbox Options plugin.


### 1.5.3
- Removed image sharpening script (caused thumbnails to now show in admin in WP3.6+).


### 1.5.2
- Added function to remove admin bar items.


### 1.5.1
- Added style shteet for SASS (styles.scss).
- Changed all instances of get_settings('home') to home_url() (get_settings is now deprecated in Wordpress).
- Changed all instances of get_bloginfo(template_url) to  get_template_directory_uri().
- Changed all instances of bloginfo(template_url) to get_template_directory_uri().
- Changed wp_load_image to wp_get_image_editor() (functions.php image sharpener).

### 1.5.0
- Updated 404 page.
- Animated 404 mascot.

### 1.4.9
- Added function to hide certain table rows from profile.php.
- Added function to sharpen uploaded images (all except original).
- Deleted dev-check.css file.

### 1.4.8
- Combined Javascripts and moved into js/strombox.js.
- Added mobile cookie name option into General Settings (functions.php).
- Bug fix in mq.css.

### 1.4.7
- Updated CSS reset

### 1.4.6
- Changed doc type to <!DOCTYPE HTML>.
- Added moderizr.
- Removed Dev Check.
- Removed 404 stylesheet reference to stormboxhosting.com.
- Removed JS for clearing inputs on focus.
- Removed clear div from below primary navigation.
- Added more touch icon meta sizes and standardised sizes.
- Updated stormbox theme icon.
- Added cookie set mobile menu helper.
- Swapped standard WP menu for Walker menu (Menus now managed in Appearance -> Menu).

### 1.4.5
- Updated dev check to have a link to recover password.

### 1.4.4
- Removed webkit input styles for IOS devices (mq.css).

### 1.4.3
- Removed css.browser.selector.js (causes blank page on iPad).
- Vieport meta content separated by commas (previously semi colons).

### 1.4.2
- Added script to show a notice for dev and staging areas, and forced login for staging area.
- Added large touch icon (for mobile devices).
- Make LINK URL default to 'none' when inserting images.
- Changed dev check URL to 127.0.0.1

### 1.4.1
- Replaced deprecated wp_specialchars() in favour of new esc_html().

### 1.4
- Made the 'edit' link static in upper right of each page (but doesn't appear on homepage).
- Removed 'edit' link from page.php, single.php and archives.php.
- Moved favicon into 'system' directory in 'images'.
- Added icon (edit.png) into stormbox/images/system.

### 1.3
- Hide admin bar from front end.

### 1.2
- Added function to remove unncessary header information.
- Added License: Proprietary to theme header.
- Updated theme icon.

### 1.1
- Included search form HTML for easier styling as opposed to default (invalid) Wordpress call.
- Mobile viewport scaling included.
- Media queries style sheet included with basic query (mq.css).
- jQuery 1.7.2 encued through functions.php (no need to include it manually any more).
- Included additional image uploaded sizes in functions.php and allowed to be selected from Admin media interface.
- Moved browser css.browser.selector.js into footer.php.
- Added jquery.hint.js into footer.php.
- Updated the search results page for better usability (search.php).
- Updated theme icon.

### 1.0
- First version of this theme.
