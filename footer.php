        <!--/ Start Footer /-->
        <footer class="footer" role="contentinfo">
            <div class="container padded">
                <div class="address">
                    <span itemscope itemtype="http://schema.org/LocalBusiness">
                        <p><span itemprop="name">© TAG Wealth International <?php echo date("Y"); ?></span></p>
                        <span itemprop="address" itemscope itemtype="http://schema.org/streetAddress">
                            <p class="small">7a Cale Street, Como | Perth 6152 | Australia</p>
                        </span>
                        <span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                            <p class="small"><span itemprop="postOfficeBoxNumber">PO Box 2160 Como</span> | <span itemprop="addressLocality">Perth</span> | <span itemprop="addressRegion">Western Australia</span> | <span itemprop="postalCode">6152</span></p>
                        </span>
                    </span>
                </div>
                <a href="http://stormbox.com.au" class="site-by-stormbox">Site by Stormbox</a>
            </div>
        </footer>
        <!--/ End Footer /-->

        <?php gravity_form(1, false, false, false, '', true); ?>

        <?php wp_footer(); ?>

        <?php if ( !is_home() ) { edit_post_link('Edit'); } ?>
    </body>
</html>
