<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

get_header(); ?>

<!--/ Start Content Block /-->
<!--/ Start Main /-->
<main id="content blog" class="content">

    <div class="news-container" id="blog">

        <!--/ Start Section /-->
        <section class="container padder news-intro">

            <h1>Blog</h1>

            <ul>
            <?php $args = array(
                'post_type'         => 'news',
                'orderby'           => 'date',
                'order'             => 'desc',
                'posts_per_page'    => 3
            );
            $query = new WP_Query($args);
            while ($query->have_posts()) : $query->the_post(); ?>
                <li>
                    <a href="<?php echo get_the_permalink(); ?>">

                        <?php if( has_post_thumbnail() ) { ?>
                            <img src="<?php echo get_the_post_thumbnail_url(get_the_ID(),'new_item'); ?>" alt="News: <?php echo the_title(); ?>" />
                        <?php } else { ?>
                            <img src="<?php echo get_template_directory_uri(); ?>/images/tag-wealth-car.jpg" alt="<?php echo the_title(); ?>" />
                        <?php } ?>

                        <strong><?php echo the_title(); ?></strong>
                        <?php
                            $content = wp_trim_words( get_the_content(), 22, '...' );
                            echo $content;
                        ?>
                        <span class="button-link">Read More</span>
                    </a>
                </li>
            <?php endwhile;
            wp_reset_postdata(); ?>
            </ul>

        </section>
        <!--/ End Section /-->
        
    </div>

</main>
<!--/ End Main /-->
<!--/ End Content Block /-->

<!--/ Start Content Block /-->
<div id="about-us" class="content-block content-block-01">
    <div class="bg"></div>
    <div class="container container-right padded">
        <?php the_field('about'); ?>
        <span class="button open-form">Enquire Now</span>
    </div>
</div>
<!--/ End Content Block /-->

<!--/ Start Content Block /-->
<div class="content-block content-block-02">
    <div class="bg"></div>
    <div class="container container-left padded">
        <?php the_field('advantage'); ?>
        <span class="button open-form">Enquire Now</span>
    </div>
</div>
<!--/ End Content Block /-->

<!--/ Start Content Block /-->
<div class="content-block content-block-03">
    <div class="bg"></div>
    <div class="container padded">
        <?php the_field('advice'); ?>
        <span class="button open-form">Enquire Now</span>
    </div>
</div>
<!--/ End Content Block /-->

<!--/ Start Content Block /-->
<div id="services" class="content-block content-block-04">
    <div class="bg"></div>
    <div class="container padded">
        <?php the_field('services'); ?>

        <div class="services-wrap">
            <ul class="services">
            <?php
            $counter = 0;

            // Loop Through Service headings
            while ( have_rows('service') ) : the_row(); ?>
                <li><a href="#service-<?php echo ++$counter; ?>" data-scroll data-options='{"offset": 80}'><?php the_sub_field('service_heading'); ?></a></li>
            <?php endwhile; ?>
            </ul>
        </div>

        <?php
        $counter = 0;

        // Loop Through Services
        while ( have_rows('service') ) : the_row(); ?>
        <div id="service-<?php echo ++$counter; ?>" class="row">
            <h3><?php the_sub_field('service_heading'); ?></h3>
            <?php the_sub_field('service_content'); ?>
        </div>
        <span class="thumb"><?php output_image('image', 'full'); ?></span>
        <?php endwhile; ?>

        <span class="button open-form">Enquire Now</span>
    </div>
</div>
<!--/ End Content Block /-->

<!--/ Start Content Block /-->
<div id="meet-the-team" class="content-block content-block-05">
    <div class="bg"></div>
    <div class="container padded">
        <?php the_field('team'); ?>

        <?php while ( have_rows('team_profile') ) : the_row(); ?>
        <?php
            $first_name = explode( ' ', trim( get_sub_field('name') ) );
            $first_name = strtolower( $first_name[0] );
        ?>
        <div class="card">
            <?php output_image('portrait', 'full'); ?>
            <p><?php the_sub_field('name'); ?></p>
            <p><?php the_sub_field('phone'); ?></p>
            <p><a href="mailto:<?php the_sub_field('email'); ?>"><?php the_sub_field('email'); ?></a></p>
            <a href="#information-section-<?php echo $first_name; ?>" class="button-more button-more-<?php echo $first_name; ?>" data-scroll data-scroll data-options='{"offset": 80}'>More Information</a>
        </div>
        <?php endwhile; ?>

        <?php while ( have_rows('team_profile') ) : the_row(); ?>
        <?php
            $first_name = explode( ' ', trim( get_sub_field('name') ) );
            $first_name = strtolower( $first_name[0] );
        ?>
        <div id="information-section-<?php echo $first_name; ?>" class="information-section information-section-<?php echo $first_name; ?>">
            <h3><?php the_sub_field('name'); ?></h3>

            <div class="left">
                <div class="image-wrap"><?php output_image('portrait', 'full'); ?></div>
                <p>Tel: <?php the_sub_field('phone'); ?></p>
                <a href="mailto:<?php the_sub_field('email'); ?>" class="button">Email <?php echo $first_name; ?></a>
            </div>
            <?php the_sub_field('description'); ?>
        </div>
        <?php endwhile; ?>
    </div>
</div>
<!--/ End Content Block /-->

<?php get_footer(); ?>
