<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

// Remove FTP nag for updates
if ( !defined( 'FS_METHOD' ) ) {
	define('FS_METHOD','direct');
}

// Disable automatic updates
if ( !defined( 'WP_AUTO_UPDATE_CORE' ) ) {
define( 'WP_AUTO_UPDATE_CORE', false );
}

// Encue Scripts and Styles
function stormbox_scripts() {
  //Main Stylesheet
  if (empty($_GET['css'])) { // Toggle stylesheet
  wp_enqueue_style('google-fonts', 'https://fonts.googleapis.com/css?family=Montserrat:400,700', false, false);
  }

  //Smooth Scroll JS
  wp_register_script('smooth-scroll-js', get_template_directory_uri() . '/js/min/smooth-scroll.min.js', false, filemtime(get_template_directory() . '/js/min/smooth-scroll.min.js'), true);
  wp_enqueue_script('smooth-scroll-js');

  //Waypoints JS
  wp_register_script('jquery.waypoints-js', get_template_directory_uri() . '/js/min/jquery.waypoints.min.js', false, filemtime(get_template_directory() . '/js/min/jquery.waypoints.min.js'), true);
  wp_enqueue_script('jquery.waypoints-js');

  //Stormbox JS
  wp_register_script('stormbox-js', get_template_directory_uri() . '/js/min/stormbox.min.js', false, filemtime(get_template_directory() . '/js/min/stormbox.min.js'), true);
  wp_enqueue_script('stormbox-js');
}
add_action( 'wp_enqueue_scripts', 'stormbox_scripts' );

// Move style.css into the footer
function presentation() { 
    
    $theme = wp_get_theme();
    $theme_version = $theme->get( 'Version' );
    ?>

    <link rel='stylesheet' id='stormbox-style-css'  href='<?php echo get_template_directory_uri(); ?>/style.css?ver=<?php echo $theme_version;?>' type='text/css' media='all' />

<?php }
add_action('wp_footer', 'presentation');

//Enque jQuery
if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);
function my_jquery_enqueue() {
  wp_deregister_script('jquery');
  wp_register_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js', false, '1.11.3', false);
  wp_enqueue_script('jquery');
}

//HTML5 Shiv
add_action('wp_head', 'conditional_ie8');
function conditional_ie8() {
$cie8 = '<!--[if lt IE 9]>
<script src="' . get_template_directory_uri() . '/js/min/html5shiv.min.js"></script>
<![endif]-->';
	echo $cie8;
	print "\n";
}

// Disable the Admin bar.
show_admin_bar(false);


// Add menu support
register_nav_menus( array(
  'primary'   => __( 'Primary navigation', 'stormbox' ),
  'sidebar'   => __( 'Sidebar navigation', 'stormbox' ),
  'secondary' => __( 'Footer navigation', 'stormbox' ),
) );

// Add HTML5 search form support
add_theme_support( 'html5', array( 'search-form' ) );

// Change the length of excerpts
function new_excerpt_length($length) {
    return 200;
}
add_filter('excerpt_length', 'new_excerpt_length');


// Featured images
add_theme_support( 'post-thumbnails' );
if ( function_exists( 'add_theme_support' ) ) {
add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 620, 400, $crop = true );


// Additional uploaded image sizes
// You can pull these images into your theme. Example: <?php the_post_thumbnail('Custom'); ? >
// add_image_size( 'Custom', 150, 150, $crop = true );
}


// Make LINK URL default to 'none' when inserting images
update_option('image_default_link_type','none');


// Remove comment count from pages in Admin
function remove_pages_count_columns($defaults) {
  unset($defaults['comments']);
  return $defaults;
}
add_filter('manage_pages_columns', 'remove_pages_count_columns');


// Remove comment count from posts in Admin
function remove_post_columns($defaults) {
  unset($defaults['comments']);
  return $defaults;
}
add_filter('manage_posts_columns', 'remove_post_columns');


// Remove tags column from posts in Admin
function remove_post_tags($defaults) {
  unset($defaults['tags']);
  return $defaults;
}
add_filter('manage_posts_columns', 'remove_post_tags');


// Remove emoji styles and scripts
function disable_emojis() {
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
}
add_action( 'init', 'disable_emojis' );

function disable_emojis_tinymce( $plugins ) {
  if ( is_array( $plugins ) ) {
    return array_diff( $plugins, array( 'wpemoji' ) );
  } else {
    return array();
  }
}



// -------------------------------------------------------------
// Stormbox Custom Theme Settings
// -------------------------------------------------------------


// Include custom JS and CSS
function admin_load_sb(){
    //wp_enqueue_script( 'custom_js', plugins_url( '/js/og-scripts.min.js', __FILE__ ), array('jquery') );
	//wp_enqueue_script('jquery');
	wp_enqueue_style( 'styles', get_bloginfo('template_directory') . '/css/stormbox-admin.css');
}
add_action('admin_enqueue_scripts', 'admin_load_sb');


// Register settings
function stormbox_settings_init(){
    register_setting( 'stormbox_settings', 'stormbox_settings' );
}


// Add settings page to menu
function sb_settings_page() {
$icon_path = get_bloginfo('template_directory') . "/images/system/admin-icon.png";
add_menu_page( __( 'Stormbox' ), __( 'Stormbox' ), 'manage_options', 'stormboxsettings', 'stormbox_settings_page' ,$icon_path);
}

// Add actions
add_action( 'admin_init', 'stormbox_settings_init' );
add_action( 'admin_menu', 'sb_settings_page' );

// Start settings page
function stormbox_settings_page() {
?>

<div class="wrap stormbox-settings">
<h2><?php _e( 'Stormbox' );?></h2>

<?php // show saved options message
if(!empty($_GET['settings-updated']) && $_GET['settings-updated'] == 'true') { ?>
	<div id="message" class="updated fade"><p><strong><?php _e( 'Options saved' ); ?></strong></p></div>
<?php } ?>

<?php if (current_user_can('administrator')) { ?>
<form method="post" action="options.php">
	<?php settings_fields( 'stormbox_settings' ); ?>
    <?php $options = get_option( 'stormbox_settings' ); ?>

    <div class="sb-floater sbcol1">
    	<h3>Presentation</h3>
        <table class="form-table">
        <tbody>

          <tr>
            <td>
            <strong><input id="stormbox_settings[remove_dashboard_widgets]" name="stormbox_settings[remove_dashboard_widgets]" type="checkbox" value="1" <?php if ( isset( $options['remove_dashboard_widgets'] ) ) { checked( '1', $options['remove_dashboard_widgets'] );} ?> /> <?php _e( 'Wordpres dashboard widgets' ); ?></strong><br />
            <span><?php _e( 'Hide the standard Wordpress dashboard widgets.' ); ?></span>
            </td>
          </tr>

          <tr>
            <td>
            <strong><input id="stormbox_settings[show_sb_widget]" name="stormbox_settings[show_sb_widget]" type="checkbox" value="1" <?php if ( isset( $options['show_sb_widget'] ) ) { checked( '1', $options['show_sb_widget'] );} ?> /> <?php _e( 'Stormbox widget' ); ?></strong><br />
            <span><?php _e( 'Show the Stormbox widget on the dashboard.' ); ?></span></td>
          </tr>

          <tr>
            <td>
            <strong><input id="stormbox_settings[sb_login_logo]" name="stormbox_settings[sb_login_logo]" type="checkbox" value="1" <?php if ( isset( $options['sb_login_logo'] ) ) { checked( '1', $options['sb_login_logo'] );} ?> /> <?php _e( 'Stormbox login logo' ); ?></strong><br />
            <span><?php _e( 'Show the Stormbox logo on the Wordpress login page.' ); ?></span></td>
          </tr>

          <tr>
            <td>
            <strong><input id="stormbox_settings[hide_user_fields]" name="stormbox_settings[hide_user_fields]" type="checkbox" value="1" <?php if ( isset( $options['hide_user_fields'] ) ) { checked( '1', $options['hide_user_fields'] );} ?> /> <?php _e( 'User profile fields' ); ?></strong><br />
            <span><?php _e( 'Hide unnessecary user profile fields.' ); ?></span></td>
          </tr>

          <tr>
            <td>
            <strong><input id="stormbox_settings[custom-visual-editor]" name="stormbox_settings[custom-visual-editor]" type="checkbox" value="1" <?php if ( isset( $options['custom-visual-editor'] ) ) { checked( '1', $options['custom-visual-editor'] );} ?> /> <?php _e( 'Custom visual editor CSS' ); ?></strong><br />
            <span><?php _e( 'Enable custom visual editor styles (css/editor-style.css).' ); ?></span></td>
          </tr>

        </tbody>
        </table>
        <p><input name="submit" id="submit" class="button button-primary" value="Save Settings" type="submit" /></p>
    </div>

    <div class="sb-floater sbcol2">
    	<h3>Remove</h3>
         <table class="form-table">
        <tbody>

        <tr>
            <td>
             <strong><input id="stormbox_settings[remove_links]" name="stormbox_settings[remove_links]" type="checkbox" value="1" <?php if ( isset( $options['remove_links'] ) ) { checked( '1', $options['remove_links'] );} ?> /> <?php _e( 'Links' ); ?></strong><br />
            <span><?php _e( 'Remove Links from nav for non-administrators.' ); ?></span></td>
          </tr>
          <tr>
            <td>
            <strong><input id="stormbox_settings[remove_comments]" name="stormbox_settings[remove_comments]" type="checkbox" value="1" <?php if ( isset( $options['remove_comments'] ) ) { checked( '1', $options['remove_comments'] );} ?> /> <?php _e( 'Comments' ); ?></strong><br />
            <span><?php _e( 'Remove Comments from nav for non-administrators.' ); ?></span></td>
          </tr>
          <tr>
            <td>
            <strong><input id="stormbox_settings[remove_appearance]" name="stormbox_settings[remove_appearance]" type="checkbox" value="1" <?php if ( isset( $options['remove_appearance'] ) ) { checked( '1', $options['remove_appearance'] );} ?> /> <?php _e( 'Appearance' ); ?></strong><br />
            <span><?php _e( 'Remove Appearance from nav for non-administrators.' ); ?></span></td>
          </tr>
          <tr>
            <td>
            <strong><input id="stormbox_settings[remove_settings]" name="stormbox_settings[remove_settings]" type="checkbox" value="1" <?php if ( isset( $options['remove_settings'] ) ) { checked( '1', $options['remove_settings'] );} ?> /> <?php _e( 'Settings' ); ?></strong><br />
            <span><?php _e( 'Remove Settings from nav for non-administrators.' ); ?></span></td>
          </tr>
          <tr>
            <td>
            <strong><input id="stormbox_settings[remove_tools]" name="stormbox_settings[remove_tools]" type="checkbox" value="1" <?php if ( isset( $options['remove_tools'] ) ) { checked( '1', $options['remove_tools'] );} ?> /> <?php _e( 'Tools' ); ?></strong><br />
            <span><?php _e( 'Remove Tools from nav for non-administrators.' ); ?></span></td>
          </tr>
          <tr>
            <td>
             <strong><input id="stormbox_settings[remove_plugins]" name="stormbox_settings[remove_plugins]" type="checkbox" value="1" <?php if ( isset( $options['remove_plugins'] ) ) { checked( '1', $options['remove_plugins'] );} ?> /> <?php _e( 'Plugins' ); ?></strong><br />
            <span><?php _e( 'Remove Plugins from nav for non-administrators.' ); ?></span></td>
          </tr>
          <tr>
            <td>
            <strong><input id="stormbox_settings[remove_forms]" name="stormbox_settings[remove_forms]" type="checkbox" value="1" <?php if ( isset( $options['remove_forms'] ) ) { checked( '1', $options['remove_forms'] );} ?> /> <?php _e( 'Gravity Forms' ); ?></strong><br />
            <span><?php _e( 'Remove Gravity Forms from nav for non-administrators.' ); ?></span></td>
          </tr>
          <tr>
            <td>
            <strong><input id="stormbox_settings[remove_users]" name="stormbox_settings[remove_users]" type="checkbox" value="1" <?php if ( isset( $options['remove_users'] ) ) { checked( '1', $options['remove_users'] );} ?> /> <?php _e( 'Users' ); ?></strong><br />
            <span><?php _e( 'Remove Users from nav for non-administrators.' ); ?></span></td>
          </tr>
          </tbody>
        </table>
          <p><input name="submit" id="submit" class="button button-primary" value="Save Settings" type="submit" /></p>
    </div>

     <div class="sb-floater sbcol3">
    	<h3>Misc</h3>
         <table class="form-table">
        <tbody>

        <tr>
            <td>
            <strong><input id="stormbox_settings[remove_update_nags]" name="stormbox_settings[remove_update_nags]" type="checkbox" value="1" <?php if ( isset( $options['remove_update_nags'] ) ) { checked( '1', $options['remove_update_nags'] );} ?> /> <?php _e( 'Disable update nags' ); ?></strong><br />
            <span><?php _e( 'Disable all update notifications for non-administrators.' ); ?></span></td>
          </tr>

          <tr>
            <td>
            <strong><input id="stormbox_settings[tinymce_tweak]" name="stormbox_settings[tinymce_tweak]" type="checkbox" value="1" <?php if ( isset( $options['tinymce_tweak'] ) ) { checked( '1', $options['tinymce_tweak'] );} ?> /> <?php _e( 'TinyMCE tweak' ); ?></strong><br />
            <span><?php _e( 'Don\'t show underline, forecolor and justifyfull.' ); ?></span></td>
          </tr>
        <tr>
            <td>
            <strong><input id="stormbox_settings[excertps_pages]" name="stormbox_settings[excertps_pages]" type="checkbox" value="1" <?php if ( isset( $options['excertps_pages'] ) ) { checked( '1', $options['excertps_pages'] );} ?> /> <?php _e( 'Allow excerpts on pages' ); ?></strong><br />
            <span><?php _e( 'Show the excerpt field on pages.' ); ?></span></td>
          </tr>
          <tr>
            <td>
            <strong><input id="stormbox_settings[hide_wp_info]" name="stormbox_settings[hide_wp_info]" type="checkbox" value="1" <?php if ( isset( $options['hide_wp_info'] ) ) { checked( '1', $options['hide_wp_info'] );} ?> /> <?php _e( 'Wordpress header info' ); ?></strong><br />
            <span><?php _e( 'Hide information that can identify Wordpress technical details.' ); ?></span></td>
          </tr>
          <tr>
            <td>
            <strong><input id="stormbox_settings[admin_bar_items]" name="stormbox_settings[admin_bar_items]" type="checkbox" value="1" <?php if ( isset( $options['admin_bar_items'] ) ) { checked( '1', $options['admin_bar_items'] );} ?> /> <?php _e( 'Admin bar' ); ?></strong><br />
            <span><?php _e( 'Remove most items from the admin bar.' ); ?></span></td>
          </tr>
          <tr>
            <td>
            <strong><input id="stormbox_settings[maintenance_mode]" name="stormbox_settings[maintenance_mode]" type="checkbox" value="1" <?php if ( isset( $options['maintenance_mode'] ) ) { checked( '1', $options['maintenance_mode'] );} ?> /> <?php _e( 'Maintenance mode' ); ?></strong><br />
            <span><?php _e( 'Enable Maintenance mode.' ); ?></span>
            </td>
          </tr>

          <tr>
          	<td class="admin-message-field">
            <strong><?php _e( 'Custom maintenance mode message' ); ?></strong><br />
            <textarea id="stormbox_settings[maintenance_message]" name="stormbox_settings[maintenance_message]" type="text" value="<?php esc_attr_e( $options['maintenance_message'] ); ?>"><?php esc_attr_e( $options['maintenance_message'] ); ?></textarea>
            <span><?php _e( 'Optional. This will override the default maintenance message.' ); ?></span>
            </td>
          </tr>


          </tbody>
        </table>
          <p><input name="submit" id="submit" class="button button-primary" value="Save Settings" type="submit" /></p>
     </div>

</form>
<?php } else {  ?>
    <div style="padding:20px; background:#fff; border:solid 1px #e5e5e5; max-width:400px; margin:20px 0 0 0; -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.04); box-shadow: 0 1px 1px rgba(0,0,0,.04);">
		<?php $sb_logo_path = get_bloginfo('template_directory') . "/images/system/stormbox.png"; ?>
        <p>
            <a href="http://stormbox.com.au/"><img src="<?php echo $sb_logo_path; ?>" style="float:left; margin:0 20px 0 0; border:none;" /></a>
            70 Brewer Street<br />
            Perth, WA 6000<br />
            Phone: 9227 0911<br />
            <a href="mailto:service@stormbox.com.au">service@stormbox.com.au</a>
        </p>
    </div>
<?php } ?>

</div>

<?php }
//sanitize and validate
function sb_options_validate( $input ) {
    global $select_options, $radio_options;
    if ( ! isset( $input['option1'] ) )
        $input['option1'] = null;
    $input['option1'] = ( $input['option1'] == 1 ? 1 : 0 );
    $input['sometext'] = wp_filter_nohtml_kses( $input['sometext'] );
    if ( ! isset( $input['radioinput'] ) )
        $input['radioinput'] = null;
    if ( ! array_key_exists( $input['radioinput'], $radio_options ) )
        $input['radioinput'] = null;
    $input['sometextarea'] = wp_filter_post_kses( $input['sometextarea'] );
    return $input;
}

// Hide WP dashboard widgets
$options = get_option('stormbox_settings'); if (is_array($options) && !empty($options['remove_dashboard_widgets']) && $options['remove_dashboard_widgets'] == true) {
	if ( !current_user_can('install_plugins') ) {
		function remove_dashboard_widgets() {
			global $wp_meta_boxes;
			remove_action( 'welcome_panel', 'wp_welcome_panel' );
			remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');
			unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
			unset($wp_meta_boxes['dashboard']['side']['core']['recent_drafts']);
			unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
			unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
			unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
			unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
			unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
			unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
			unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);
			unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
		}
		add_action('wp_dashboard_setup', 'remove_dashboard_widgets' );
	}
}


// Show the Stormbox widget
$options = get_option('stormbox_settings'); if (is_array($options) && !empty($options['show_sb_widget']) && $options['show_sb_widget'] == true) {
	function sb_dashboard_widget_function() {
	// Display whatever it is you want to show
	$sb_logo_path = get_bloginfo('template_directory') . "/images/system/stormbox.png"; ?>
	<p>
        <a href="http://stormbox.com.au/"><img src="<?php echo $sb_logo_path; ?>" style="float:left; margin:0 20px 0 0; border:none;" /></a>
        70 Brewer Street<br />
        Perth, WA 6000<br />
        Phone: 9227 0911<br />
        <a href="mailto:service@stormbox.com.au">service@stormbox.com.au</a>
    </p>
<?php }
function add_sb_dashboard_widgets() {
	wp_add_dashboard_widget('sb_dashboard_widget', 'Stormbox', 'sb_dashboard_widget_function');
}
add_action('wp_dashboard_setup', 'add_sb_dashboard_widgets' );
}


// Remove update nags
$options = get_option('stormbox_settings'); if (is_array($options) && !empty($options['remove_update_nags']) && $options['remove_update_nags'] == true) {
	if ( !current_user_can('install_plugins') ) {

	function remove_core_updates(){
		global $wp_version;return(object) array('last_checked'=> time(),'version_checked'=> $wp_version,);
	}
	add_filter('pre_site_transient_update_core','remove_core_updates');
	add_filter('pre_site_transient_update_plugins','remove_core_updates');
	add_filter('pre_site_transient_update_themes','remove_core_updates');
	}
}


// TinyMCE tweaks
$options = get_option('stormbox_settings'); if (is_array($options) && !empty($options['tinymce_tweak']) && $options['tinymce_tweak'] == true) {
	function customformatTinyMCE($init) {
	// Add elements you want to show in the 'format' dropdown
	$init['theme_advanced_blockformats'] = 'p,h2,h3,h4,h5';

	// Remove elements you don't want to show in the 'format' dropdown
	$init['theme_advanced_disable'] = 'underline,forecolor,justifyfull';

	return $init;
	}
	add_filter('tiny_mce_before_init', 'customformatTinyMCE' );
}


// Stormbox login logo
$options = get_option('stormbox_settings'); if (is_array($options) && !empty($options['sb_login_logo']) && $options['sb_login_logo'] == true) {
	function sb_login_logo() {
	$logo_path = get_template_directory_uri() . "/images/system/stormbox.png";
	echo '<style type="text/css">
		h1 { background-image:url(' . $logo_path . ') !important; position:relative; top:-15px; height:70px !important; width:173px !important; left:70px; display:block; }
		h1 a {visibility:hidden;}
	</style>';
	}
	add_action('login_head', 'sb_login_logo');
}


// Remove Links
$options = get_option('stormbox_settings'); if (is_array($options) && !empty($options['remove_links']) && $options['remove_links'] == true) {
	if ( !current_user_can('install_plugins') ) {
		function hide_links () {
		global $menu;
		$restricted = array(('Links'));
			end ($menu);
		while (prev($menu)){
			$value = explode(' ',$menu[key($menu)][0]);
		if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){unset($menu[key($menu)]);}
			}
		}
		add_action('admin_menu', 'hide_links');
	}
}


// Remove Comments
$options = get_option('stormbox_settings'); if (is_array($options) && !empty($options['remove_comments']) && $options['remove_comments'] == true) {
	if ( !current_user_can('install_plugins') ) {
		function hide_comments () {
		global $menu;
		$restricted = array(('Comments'));
			end ($menu);
		while (prev($menu)){
			$value = explode(' ',$menu[key($menu)][0]);
		if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){unset($menu[key($menu)]);}
			}
		}
		add_action('admin_menu', 'hide_comments');
	}
}

// Remove Appearance
$options = get_option('stormbox_settings'); if (is_array($options) && !empty($options['remove_appearance']) && $options['remove_appearance'] == true) {
	if ( !current_user_can('install_plugins') ) {
		function hide_apearance () {
		global $menu;
		$restricted = array(('Appearance'));
			end ($menu);
		while (prev($menu)){
			$value = explode(' ',$menu[key($menu)][0]);
		if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){unset($menu[key($menu)]);}
			}
		}
		add_action('admin_menu', 'hide_apearance');
	}
}

// Remove Settings
$options = get_option('stormbox_settings'); if (is_array($options) && !empty($options['remove_settings']) && $options['remove_settings'] == true) {
	if ( !current_user_can('install_plugins') ) {
		function hide_settings () {
		global $menu;
		$restricted = array(('Settings'));
			end ($menu);
		while (prev($menu)){
			$value = explode(' ',$menu[key($menu)][0]);
		if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){unset($menu[key($menu)]);}
			}
		}
		add_action('admin_menu', 'hide_settings');
	}
}

// Remove Tools
$options = get_option('stormbox_settings'); if (is_array($options) && !empty($options['remove_tools']) && $options['remove_tools'] == true) {
	if ( !current_user_can('install_plugins') ) {
		function hide_tools () {
		global $menu;
		$restricted = array(('Tools'));
			end ($menu);
		while (prev($menu)){
			$value = explode(' ',$menu[key($menu)][0]);
		if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){unset($menu[key($menu)]);}
			}
		}
		add_action('admin_menu', 'hide_tools');
	}
}

// Remove Plugins
$options = get_option('stormbox_settings'); if (is_array($options) && !empty($options['remove_plugins']) && $options['remove_plugins'] == true) {
	if ( !current_user_can('install_plugins') ) {
		function hide_plugins () {
		global $menu;
		$restricted = array(('Plugins'));
			end ($menu);
		while (prev($menu)){
			$value = explode(' ',$menu[key($menu)][0]);
		if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){unset($menu[key($menu)]);}
			}
		}
		add_action('admin_menu', 'hide_plugins');
	}
}

// Remove Forms (gravity forms)
$options = get_option('stormbox_settings'); if (is_array($options) && !empty($options['remove_forms']) && $options['remove_forms'] == true) {
	if ( !current_user_can('install_plugins') ) {
		function hide_forms() {
				remove_menu_page( 'gf_edit_forms' ); // this is the pages url
		}
		add_action( 'admin_menu', 'hide_forms', 9999 );

		function hide_forms_menu() {
				remove_submenu_page( 'gf_edit_forms', 'gf_help' );
		}
		add_action( 'admin_menu', 'hide_forms_menu', 9999 );
	}
}

// Remove Users
$options = get_option('stormbox_settings'); if (is_array($options) && !empty($options['remove_users']) && $options['remove_users'] == true) {
	if(!is_admin()) {
		function hide_users () {
		global $menu;
		$restricted = array(('Users'));
			end ($menu);
		while (prev($menu)){
			$value = explode(' ',$menu[key($menu)][0]);
		if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){unset($menu[key($menu)]);}
			}
		}
		add_action('admin_menu', 'hide_users');
	}
}

// Hide unnessecary user profile fields
$options = get_option('stormbox_settings'); if (is_array($options) && !empty($options['hide_user_fields']) && $options['hide_user_fields'] == true) {
	global $pagenow;
	if ($pagenow == 'profile.php') {

	function admin_register_head() {
	echo '<!--/ Add classes to table rows /-->
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
		<script type="text/javascript">
		$(document).ready(function() {
		$(".form-table tr").each(function(i) {
		$(this).addClass("row_" + (i+1));
		});
		});
	</script>

	<style type="text/css">
		.row_1, .row_2, .row_5, .row_6, .row_8, .row_9, .row_10, .row_11, .row_14, .row_15, .row_16 {
		display:none;
		}
	</style>';
	}
	if (is_admin()) add_action('admin_head', 'admin_register_head');
	}
}

// Show the excerpt field on pages
$options = get_option('stormbox_settings'); if (is_array($options) && !empty($options['excertps_pages']) && $options['excertps_pages'] == true) {
	if ( function_exists('add_post_type_support') )  {
	add_action('init', 'add_page_excerpts');
	function add_page_excerpts() {
		add_post_type_support( 'page', 'excerpt' );
	  }
	}
}


// Hide information that can identify Wordpress technical details
$options = get_option('stormbox_settings'); if (is_array($options) && !empty($options['hide_wp_info']) && $options['hide_wp_info'] == true) {
	function remove_header_info() {
		remove_action('wp_head', 'rsd_link');
		remove_action('wp_head', 'wlwmanifest_link');
		remove_action('wp_head', 'wp_generator');
		remove_action('wp_head', 'start_post_rel_link');
		remove_action('wp_head', 'index_rel_link');
		remove_action('wp_head', 'adjacent_posts_rel_link');
	}
	add_action('init', 'remove_header_info');
}

//Remove items from the admin bar
$options = get_option('stormbox_settings'); if (is_array($options) && !empty($options['admin_bar_items']) && $options['admin_bar_items'] == true) {
	function remove_admin_bar_links() {
	global $wp_admin_bar;
	$wp_admin_bar->remove_menu('wp-logo');          // Remove the WordPress logo
	$wp_admin_bar->remove_menu('about');            // Remove the about WordPress link
	$wp_admin_bar->remove_menu('wporg');            // Remove the WordPress.org link
	$wp_admin_bar->remove_menu('documentation');    // Remove the WordPress documentation link
	$wp_admin_bar->remove_menu('support-forums');   // Remove the support forums link
	$wp_admin_bar->remove_menu('feedback');         // Remove the feedback link
	//    $wp_admin_bar->remove_menu('site-name');        // Remove the site name menu
	//    $wp_admin_bar->remove_menu('view-site');        // Remove the view site link
	$wp_admin_bar->remove_menu('updates');          // Remove the updates link
	$wp_admin_bar->remove_menu('comments');         // Remove the comments link
	$wp_admin_bar->remove_menu('new-content');      // Remove the content link
	$wp_admin_bar->remove_menu('w3tc');             // If you use w3 total cache remove the performance link
	//    $wp_admin_bar->remove_menu('my-account');       // Remove the user details tab
	}
	add_action( 'wp_before_admin_bar_render', 'remove_admin_bar_links' );
}


// Show a nicer font in the visual editor
$options = get_option('stormbox_settings'); if (is_array($options) && !empty($options['custom-visual-editor']) && $options['custom-visual-editor'] == true) {
	function acme_load_editor_style() {
	  add_editor_style( get_template_directory_uri() . '/css/editor-style.css' );
	}
	add_action( 'after_setup_theme', 'acme_load_editor_style' );
}


// Enable maintenance mode, use custom message if specified
$options = get_option('stormbox_settings'); if (is_array($options) && !empty($options['maintenance_mode']) && $options['maintenance_mode'] == true) {
	function wp_maintenance_mode(){
		if(!current_user_can('edit_themes') || !is_user_logged_in()){
			$options = get_option('stormbox_settings'); if (is_array($options) && !empty($options['maintenance_message']) && $options['maintenance_message'] == true) {
				$custom_message = $options['maintenance_message'];
				wp_die('<h1 style="color:red">Maintenance in progress</h1><br />' . $custom_message . '');
			} else {
				wp_die('<h1 style="color:red">Maintenance in progress</h1><br />We are performing scheduled maintenance and will be back on-line shortly. Stay tuned.');
			}
		}
	}
	add_action('get_header', 'wp_maintenance_mode');
}


// Responsive YouTube embeds
add_filter('embed_oembed_html', 'my_embed_oembed_html', 99, 4);
function my_embed_oembed_html($html, $url, $attr, $post_id) {
  return '<div class="video-container">' . $html . '</div>';
}


// Echo out a custom image field
function output_image( $field, $size) {
    $image = get_sub_field($field);

    if( $image ) {
        echo wp_get_attachment_image( $image, $size );
    }
}


// Change gravity forms  validation message
add_filter( 'gform_validation_message', 'change_message', 10, 2 );
function change_message( $message, $form ) {
    return '<div class="validation_error">Oh dear! Looks like you forgot to fill out some of the required fields. Please check and try again.</div>';
}


// Change gravity forms AJAX spinner
add_filter( 'gform_ajax_spinner_url', 'tgm_io_custom_gforms_spinner' );
function tgm_io_custom_gforms_spinner( $src ) {
    return get_stylesheet_directory_uri() . '/images/system/loading.svg';
}
