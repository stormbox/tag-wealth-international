<!DOCTYPE HTML>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><?php bloginfo('name'); ?></title>

        <!--/ Mobile Viewport Scale /-->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!--/ Icons /-->
        <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/system/favicon.png">
        <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/images/system/touch-icon.png">
        <link rel="icon" sizes="192x192" href="<?php echo get_template_directory_uri(); ?>/images/system/icon.png">

        <!--/ Chrome Theme /-->
        <meta name="theme-color" content="#cb4e28">

        <meta name="mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-capable" content="yes">

        <style type="text/css">
            #fouc::before {
                content: "";
                display: block;
                width: 100%;
                height: 100%;
                background: #fff;
                position: fixed;
                top: 0;
                left: 0;
                z-index: 99999999999999999 !important;
            }
        </style>

        <?php get_template_part('critical-css');
        wp_head(); ?>

        <!--[if lte IE 10]>
        Fix for IE10 and below not supporting pointer-events
        <style>
            .gform_wrapper {
                z-index: -1;
            }
            .form-open {
                z-index: 15 !important;
            }
        </style>
        <![endif]-->

        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-46908825-2', 'auto');
          ga('send', 'pageview');

        </script>

    </head>
    <body id='fouc'>

        <script>
            $(document).ready(function() {
                $('body').removeAttr('id');    
            });
        </script>

        <header class="header" role="banner"> 
            <div class="bg"></div>

            <!--/ Start Primary Navigation /-->
            <nav class="primary-navigation" role="navigation">
                <ul id="menu-primary-navigation" class="menu">
                    <li class="menu-item"><a href="<?php echo home_url();?>#about-us">About Us</a></li>
                    <li class="menu-item"><a href="<?php echo home_url();?>#services">Services</a></li>
                    <li class="menu-item"><a href="<?php echo home_url();?>#meet-the-team">Meet The Team</a></li>
                    <li class="menu-item"><a href="<?php echo home_url();?>#blog">Blog</a></li>
                    <li class="menu-enquire menu-item"><a>Enquire</a></li>
                    <li class="menu-phone menu-item"><a href="tel:+61409884134">+61409884134</a></li>
                </ul>
            </nav>
            <!--/ End Primary Navigation /-->

            <!--/ Start CTA /-->
                <?php get_template_part('includes/main-cta'); ?>
            <!--/ End CTA /-->

            <div class="container padded">
                <h1 class="rotated">Providing <br> offshore <br> financial advice <br> for expats</h1>
            </div>
        </header>
