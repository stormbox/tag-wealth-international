// Add class on scroll
$('body').waypoint(function(direction) {
    $('body').toggleClass('scrolled');
});

// Add class to services when scrolled past first service
$('#service-1').waypoint(function(direction) {
    $('.services, .services-wrap').toggleClass('scrolled');
}, {
    offset: '80px'
});

$('.content-block-05').waypoint(function(direction) {
    $('.services, .services-wrap').toggleClass('scrolled');
}, {
    offset: '500px'
});

// Show the form
$('.open-form, .menu-enquire').on('click', function() {
    $('.gform_wrapper').addClass('form-open');
});

$(document).bind('gform_post_render', function(){
    $('.form-contact').prepend('<span class="close-form">x</span>');
});

// Hide the form
$('.gform_wrapper').on('click', function(e) {
    if( e.target != this )
    return;
    $(this).removeClass('form-open');
});

$(document).ready(function(){
    $('body').on('click', '.close-form', function(){
        $('.gform_wrapper').removeClass('form-open');
    });
});

// Close form on ESC keypress
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        $('.gform_wrapper').removeClass('form-open');
    }
});

// Initialize Smooth Scroll
$('.scrollable a').attr('data-scroll', '');
smoothScroll.init({
    offset: -140
});

// Show More Info (Cards)
var names = ['andy', 'tony'];
names.forEach(function (item) {
    $('.button-more-' + item).on('click', function() {
        $('.information-section').removeClass('visible');
        $('.information-section-' + item).addClass('visible');
    });
});
