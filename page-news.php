<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

get_header(); ?>

<!--/ Start Main /-->
<main id="content" class="content">

    <div class="news-container">

        <!--/ Start Section /-->
        <section class="container padder news-intro">

            <h1>Blog</h1>

            <ul>
            <?php $args = array(
                'post_type'         => 'news',
                'orderby'           => 'date',
                'order'             => 'desc',
                'posts_per_page'    => -1
            );
            $query = new WP_Query($args);
            while ($query->have_posts()) : $query->the_post(); ?>
                <li>
                    <a href="<?php echo get_the_permalink(); ?>">

                        <?php if( has_post_thumbnail() ) { ?>
                            <img src="<?php echo get_the_post_thumbnail_url(get_the_ID(),'new_item'); ?>" alt="News: <?php echo the_title(); ?>" />
                        <?php } else { ?>
                            <img src="<?php echo get_template_directory_uri(); ?>/images/tag-wealth-car.jpg" alt="<?php echo the_title(); ?>" />
                        <?php } ?>

                        <strong><?php echo the_title(); ?></strong>
                        <?php
                            $content = wp_trim_words( get_the_content(), 22, '...' );
                            echo $content;
                        ?>
                        <span class="button-link">Read More</span>
                    </a>
                </li>
            <?php endwhile;
            wp_reset_postdata(); ?>
            </ul>

        </section>
        <!--/ End Section /-->
        
    </div>

</main>
<!--/ End Main /-->

<?php get_footer(); ?>