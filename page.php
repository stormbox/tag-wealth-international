<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

get_header(); ?>

<div class="content">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <?php endwhile; endif; ?>
</div>

<!--/ Start Content Block /-->
<div class="content-block content-block-05">
    <div class="bg"></div>
    <div class="container padded">
		<h2><?php the_title(); ?></h2>
	    <?php the_content(); ?>
    </div>
</div>
<!--/ End Content Block /-->


<?php get_footer(); ?>
