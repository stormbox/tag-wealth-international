<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

get_header(); ?>



<!--/ Start Main /-->
<main id="content" class="content">

    <div class="container">

        <?php if (have_posts()) : ?>
        <h1>Search Results for <span><?php echo $_GET['s']; ?></span></h1>

        <ul id="search_results">
            <?php while (have_posts()) : the_post(); ?>
            <li>
                <span>
                    <strong><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></strong>
                    <?php echo substr(get_the_excerpt(), 0,180); ?>...
                    <a href="<?php the_permalink() ?>">view this page</a>
                </span>
            </li>
            <?php endwhile; ?>
        </ul>

        <?php else : ?>
        <h2>Eep! What did you search for?</h2>
        <p>It appears we couldn't find anything for the search term <span><?php echo $_GET['s']; ?></span>. Perhaps try a different search term?</p>
        <?php endif; ?>

    </div>

</main>
<!--/ End Main /-->

<?php get_footer(); ?>