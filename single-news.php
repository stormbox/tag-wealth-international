<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

get_header(); ?>

<!--/ Start Main /-->
<main id="content" class="content" itemscope itemtype="https://schema.org/Article">
    <?php 
    if (have_posts()) : while (have_posts()) : the_post();
    if( has_post_thumbnail() ) {
        $thumbnail  = get_the_post_thumbnail_url(get_the_ID(),'thumbnail');
    }
    ?>

<div class="inner-blog-bg"></div>

    <!--/ Start Article /-->
    <article id="post-<?php the_ID(); ?>" class="blog-post-single" itemscope itemprop="mainEntityOfPage">    

        <div class="container inner-news-container">

            <!--/ Start Blog Post /-->
            <div>
                <div class="entry-header">

                    <h2 class="news-intro" itemprop="headline"><?php the_title(); ?></h2>
                    
                    <!-- <span class="publish-details">
                        <time class="posted-on published" datetime="<?php the_time('c'); ?>" itemprop="datePublished"><?php echo get_the_date( 'D M j, Y' ); ?></time> by 
                        <?php echo get_the_author_meta( 'first_name' ); ?> <?php echo get_the_author_meta( 'last_name' ); ?>
                    </span> -->

                    <meta itemprop="author" content="<?php echo get_bloginfo('name'); ?>"/>
                    <meta itemprop="publisher" content="<?php echo get_bloginfo('name'); ?>"/>
                    <meta itemprop="datePublished" content="<?php the_time('c'); ?>"/>
                    <meta itemprop="dateModified" content="<?php the_modified_date('Y/m/d g:i:s'); ?>"/>
                </div>

                <div class="entry-content" role="main" itemprop="articleBody">
                    <?php the_content(); ?>
                    <p><a href="<?php echo home_url();?>" class="back">Back to Home</a></p>
                </div>
            </div>
            <!--/ End Blog Post /-->

        </div>

    </article>
    <!--/ End Article /-->

    <?php endwhile; endif; ?>
</main>
<!--/ End Main /-->

<?php get_footer(); ?>
